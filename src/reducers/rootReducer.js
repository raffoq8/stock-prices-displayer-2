const initState = {
  companies: []
};

const rootReduсer = (state = initState, { type, companies }) => {

  if (type === 'UPDATE_STORAGE') {

    const newCompanies = companies.map((company) => {
      const existingCompany = state.companies.find(stateCompany => stateCompany.id === company.id);
      let newHistory = [company.price];

      if (!existingCompany){
        return {
          ...company,
          history: newHistory,
          prevPrice: undefined,
        }
      } else {
        newHistory = existingCompany.history.concat(newHistory);
        if (newHistory.length === 21){
          newHistory.shift();
        }
      }
      
      return {
        ...company,
        history: newHistory,
        prevPrice: existingCompany.price,
      }
    });

    return {
      ...state,
      companies: newCompanies,
    }
  }

  return state
};

export default rootReduсer;