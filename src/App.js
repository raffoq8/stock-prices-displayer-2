import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import io from 'socket.io-client';
import { connect } from 'react-redux'

import Detailes from './components/details'
import PricesTable from './components/table'


class App extends Component {

  componentDidMount() {
    const socket = io.connect('http://localhost:4000/');

    socket.on('stockPrices', ({ companiesCurrentState }) => {
      this.props.updateReduxStorage(companiesCurrentState);
    });
  }

  render() {
    return (
      <div className="App">
        <Router>
          <Switch>
            <Route exact path="/" component={PricesTable} />
            <Route exact path="/details/:comp_id" component={Detailes} />
          </Switch>
        </Router>
      </div>
    )
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    updateReduxStorage: companies => dispatch({ type: 'UPDATE_STORAGE', companies }),
  }
}

export default connect(undefined, mapDispatchToProps)(App);