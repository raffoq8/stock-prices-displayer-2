import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Table, TableBody, TableCell, TableContainer, TableHead, TableRow, Paper, Typography, Button } from '@material-ui/core'

class Details extends Component {

  backToTable = () => {
    const { history } = this.props;
    history.push('/');
  }

  render() {
    const { company } = this.props;

    const cells = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19];
    const header = cells.map(cell => {
      return (
        <TableCell key={cell}>{cell + 1}</TableCell>
      )
    });

    let body = [];

    if (company) {
      body = cells.map(cell => {

        const price = company.history.length > cell ? company.history[cell] : '-';

        return (
          <TableCell key={cell}>{price}</TableCell>
        )
      })
    }

    const table = company ? (
      <div className="details-content">
        <Typography align="center" color="primary" variant="h4">Last 20 price changes for "{company.name}"</Typography>

        <div className="table-wrapper">
          <TableContainer component={Paper}>
            <Table aria-label="simple table">

              <TableHead>
                <TableRow>
                  {header}
                </TableRow>
              </TableHead>

              <TableBody>
                <TableRow>
                  {body}
                </TableRow>
              </TableBody>

            </Table>
          </TableContainer>
          <Button onClick={this.backToTable}>Back</Button>
        </div>

      </div>
    ) : (<div>Loading...</div>)

    return (
      <>
        {table}
      </>
    )
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    company: state.companies.find(company => company.id == ownProps.match.params.comp_id),
  }
}

export default connect(mapStateToProps)(Details);