import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Table, TableBody, TableCell, TableContainer, TableHead, TableRow, Paper, Typography } from '@material-ui/core'

class PricesTable extends Component {

  seeDetailes = (companyId, companyName) => {
    const { history } = this.props;
    history.push('/details/' + companyId, { companyName });
  }

  render() {
    const { companies } = this.props;

    const tableBody = companies.map(company => {
      return (
        <TableRow onClick={() => { this.seeDetailes(company.id, company.name) }} key={company.id}>
          <TableCell>{company.name}</TableCell>
          <TableCell>{company.price}</TableCell>
          <TableCell>{company.prevPrice ? company.prevPrice : 'no data yet'}</TableCell>
        </TableRow>
      )
    })

    const table = (
      <div className="table-wrapper">
        <TableContainer component={Paper}>
          <Table aria-label="simple table">

            <TableHead>
              <TableRow>
                <TableCell>Companies</TableCell>
                <TableCell>Price</TableCell>
                <TableCell>Previous price</TableCell>
              </TableRow>
            </TableHead>

            <TableBody>
              {tableBody}
            </TableBody>

          </Table>
        </TableContainer>
      </div>
    )

    const loader = (
      <div>Loading...</div>
    )

    return (
      <div className="table-content">
        <Typography align="center" color="primary" variant="h3">Stock prices displayer</Typography>
        {companies.length ? table : loader}
      </div>
    )
  }
};

const mapStateToProps = (state) => {
  return {
    companies: state.companies
  }
};

export default connect(mapStateToProps)(PricesTable);