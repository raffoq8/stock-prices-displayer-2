
//There is a server which provides fake stock prices data

const express = require('express');
const socket = require('socket.io');
const fakeStockGenetator = require('fake-stock-market-generator');

//App setup
const app = express();
const server = app.listen(4000, () => {
  console.log('server started on port 4000');
});

//Socket setup
const io = socket(server);

io.on('connection', (socket) => {
  console.log(`made web socket connection, socket id: ${socket.id}`);
});


//Create 5 companies with fake names and fake stock prices
let companies = [];
for (let i = 0; i < 5; i++) {
  companies[i] = fakeStockGenetator.generateStockData(1000);
}

//Imitate stock price updating
//Prened that we are listening to update event and then invoke callback function
//to send this data to client side
let iteration = -1;
const intervalId = setInterval(() => {
  iteration++;
  let companiesCurrentState = [];

  companies.forEach((company, index) => {
    const currentCompany = {
      name: company.companyName,
      price: company.priceData[iteration].price,
      id: index,
    };
    companiesCurrentState.push(currentCompany);
  });

  io.sockets.emit('stockPrices', { companiesCurrentState })

}, 3000);